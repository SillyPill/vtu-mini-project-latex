# VTU Mini Project Template

A Latex template for VTU mini projects. 

## How to use it

1. Edit ```globals.tex``` and change the values
1. Replace ```images/collegelogo.png``` with your college's Logo
1. Bunch of smaller changes here and there ( such as college address etc. ) will be necessary.
1. Delete ```tutorial.tex``` and the corresponding ```include``` in ```report.tex```

## Some resources to learn LaTeX

- [Luke's LaTeX playlist on YouTube](https://www.youtube.com/playlist?list=PL-p5XmQHB_JSQvW8_mhBdcwEyxdVX0c1T)

- [A more comprehensive course on LaTex.](http://www.science.uva.nl/onderwijs/lesmateriaal/latex/latexcourse.pdf)
  This could be useful for modifying the template or **Advance Stuff**

- [LaTeX for beginners](http://www.docs.is.ed.ac.uk/skills/documents/3722/3722-2014.pdf)

## Acknowledgement

I referred to (stole) a lot of 'code' from [Amruth Pillai's repository](https://github.com/AmruthPillai/VTU-DSCE-Project-Report-Template)
